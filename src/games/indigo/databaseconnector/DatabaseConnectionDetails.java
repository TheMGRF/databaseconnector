package games.indigo.databaseconnector;

public class DatabaseConnectionDetails {
	
	private String id, ip, database, username, password = null;
	private int port;
	
	public DatabaseConnectionDetails(String id, String ip, int port, String database, String username, String password) {
		this.id = id;
		this.ip = ip;
		this.port = port;
		this.database = database;
		this.username = username;
		this.password = password;
	}
	
	public String getID() {
		return id;
	}
	
	public String getIP() {
		return ip;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getDatabase() {
		return database;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
}
