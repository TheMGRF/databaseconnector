package games.indigo.databaseconnector;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class DatabaseConnector {
	
	// this is the API that other plugins interact with
	Map<String, Database> databases;
	
	public DatabaseConnector(Logger logger, DatabaseConnectionDetails...details) {
		databases = new HashMap<String, Database>();
		
		for (DatabaseConnectionDetails d : details) {
			Database database = new Database(d.getIP(), d.getPort(), d.getDatabase(), d.getUsername(), d.getPassword());
			databases.put(d.getID(), database);
			if (database.isConnected()) logger.info("Database ID '" + d.getID() + "' connected.");
			else logger.severe("Database ID '" + d.getID() + "' failed to connect!");
		}
	}
	
	public Connection getConnection(String database) throws SQLException {
		if (databases.containsKey(database)) return databases.get(database).getConnection();
		return null;
	}
	
	public void shutdown() {
		for (Database database : databases.values()) {
			if (database.isConnected()) database.shutdown();
		}
	}
	
}
