package games.indigo.databaseconnector;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public class DatabaseConnectorMain extends JavaPlugin {
	
	private final String configIp = "ip";
	private final String configPort = "port";
	private final String configDatabase = "database";
	private final String configUsername = "username";
	private final String configPassword = "password";
	
	private DatabaseConnector databaseConnector;
	
	public void onEnable() {
		
		saveDefaultConfig();
		
		// load databases from config
		List<DatabaseConnectionDetails> detailsList = new ArrayList<DatabaseConnectionDetails>();
		for (String id : getConfig().getKeys(false)) {
			
			String prefix = id + ".";
			
			String ip = getConfig().getString(prefix + configIp);
			int port = getConfig().getInt(prefix + configPort);
			String database = getConfig().getString(prefix + configDatabase);
			String username = getConfig().getString(prefix + configUsername);
			String password = getConfig().isSet(prefix + configPassword) ? getConfig().getString(prefix + configPassword) : "";
			
			DatabaseConnectionDetails details = new DatabaseConnectionDetails(id, ip, port, database, username, password);
			detailsList.add(details);
		}
		
		// connect to databases
		databaseConnector = new DatabaseConnector(getLogger(), detailsList.toArray(new DatabaseConnectionDetails[0]));
		
		// register API in bukkit
		getServer().getServicesManager().register(DatabaseConnector.class, databaseConnector, this, ServicePriority.Normal);
		
	}
	
	public void onDisable() {
		databaseConnector.shutdown();
	}
	
}
